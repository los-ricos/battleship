/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

public class Coordenada{
    private int x;
    private int y;

/*-----------------------------------------------------------------------
	Coordenada
	Entradas: No posee
    Salidas: No posee
    Restricciones: no posee
	Funcionamiento: 
        - Constructor vacio con valores predeterminados
-----------------------------------------------------------------------*/
    public Coordenada(){
        x = 0;
        y = 0;
    }

/*-----------------------------------------------------------------------
	Coordenada
	Entradas: dos enteros
    Salidas: No posee
    Restricciones: deben ser enteros
	Funcionamiento: 
        - Constructor con parametros que agrega dos enteros al objeto
-----------------------------------------------------------------------*/
    public Coordenada(int x, int y){
        this.x = x;
        this.y = y;
    }

/*-----------------------------------------------------------------------
	getX
	Entradas: No posee
    Salidas: un entero
    Restricciones: No posee
	Funcionamiento: 
        - getter de x
-----------------------------------------------------------------------*/
    public int getX(){
        return this.x;

    }

/*-----------------------------------------------------------------------
	getY
	Entradas: No posee
    Salidas: un entero
    Restricciones: No posee
	Funcionamiento: 
        - getter de y
-----------------------------------------------------------------------*/
    public int getY(){
        return this.y;

    }
    @Override 
    public String toString(){


        return String.format("["+x+","+y+"]");



    }
}