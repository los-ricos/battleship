/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

import java.util.ArrayList;
import java.util.Scanner;

public class Jugador{
    private String nombre;
    private int clave;
    private Tablero tablerin = new Tablero();
    private Tablero tableroDisparo = new Tablero();
    private ArrayList<Coordenada> coordenadasBarcos = new ArrayList<Coordenada>();
    private ArrayList<Coordenada> barcosHundidos=new ArrayList<Coordenada>();
    private ArrayList<Barco> barcosVivos = new ArrayList<Barco>();
    

/*-----------------------------------------------------------------------
	Jugador
	Entradas: No recibe parámetros
    Salidas: No posee salidas
    Restricciones: No posee
	Funcionamiento: 
        - Constructor vacio con valores por defecto
-----------------------------------------------------------------------*/
    public Jugador(){
        nombre = "";
        clave = 0;
    }

/*-----------------------------------------------------------------------
	Jugador
	Entradas: un número y un String
    Salidas: No posee salidas
    Restricciones: debe ser un entero y un string
	Funcionamiento: 
        - Constructor con parametros
        - almacena la clave y el nombre de un Jugador
-----------------------------------------------------------------------*/
    public Jugador(int clave, String nombre){
        this.clave = clave;
        this.nombre = nombre;

    }

/*-----------------------------------------------------------------------
	getNombre
	Entradas: No recibe
    Salidas: Un String
    Restricciones: No posee
	Funcionamiento: 
        - Getter de nombre
-----------------------------------------------------------------------*/
    public String getNombre() {
        return this.nombre;
    }

/*-----------------------------------------------------------------------
	setNombre
	Entradas: Un string
    Salidas: No posee
    Restricciones: Debe ser un string
	Funcionamiento: 
        - Setter de nombre
-----------------------------------------------------------------------*/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

/*-----------------------------------------------------------------------
	getClave
	Entradas: No recibe
    Salidas: Un numero entero
    Restricciones: No posee
	Funcionamiento: 
        - Getter de clave
-----------------------------------------------------------------------*/
    public int getClave() {
        return this.clave;
    }
    
/*-----------------------------------------------------------------------
	setClave
	Entradas: Un entero
    Salidas: No posee
    Restricciones: Debe ser un entero
	Funcionamiento: 
        - Setter de clave
-----------------------------------------------------------------------*/
    public void setClave(int clave){
        this.clave = clave;
    }

/*-----------------------------------------------------------------------
	insertarBarco
	Entradas: Un Objeto de tipo barco
    Salidas: No posee
    Restricciones: Debe ser tipo barco
	Funcionamiento: 
        - Inserta un barco al arrayList
-----------------------------------------------------------------------*/
    public void insertarBarco(Barco barco){
        barcosVivos.add(barco);
    }

/*-----------------------------------------------------------------------
	eliminarBarco
	Entradas: Un Objeto de tipo barco
    Salidas: No posee
    Restricciones: Debe ser tipo barco
	Funcionamiento: 
        - Elimina un barco del arrayList
-----------------------------------------------------------------------*/
    public void eliminarBarco(Barco barco){
        barcosVivos.remove(barco);
    }
/*-----------------------------------------------------------------------
	obtenerBarco
	Entradas: Un entero
    Salidas: Un objeto tipo Barco
    Restricciones: Debe ser entero
	Funcionamiento: 
        - Obtiene un barco del arrayList por posicion
-----------------------------------------------------------------------*/
    public Barco obtenerBarco(int pos){
      return barcosVivos.get(pos);
    }
    
/*-----------------------------------------------------------------------
	esVacia
	Entradas: No posee
    Salidas: Un booleano
    Restricciones: No posee
	Funcionamiento: 
        - Define si el arrayList está vacio o no mediante un booleano
-----------------------------------------------------------------------*/
    public boolean esVacia(){
        return barcosVivos.isEmpty();
    }

/*-----------------------------------------------------------------------
	tamañoLista
	Entradas: No posee
    Salidas: Un entero
    Restricciones: No posee
	Funcionamiento: 
        - Envia el tamaño actual del ArrayList
-----------------------------------------------------------------------*/
    public int tamañoLista(){
      return barcosVivos.size();
    }

/*-----------------------------------------------------------------------
	getTablerin
	Entradas: No posee
    Salidas: Un objeto de tipo tablero
    Restricciones: No posee
	Funcionamiento: 
        - Retorna un objeto de tipo tablero
-----------------------------------------------------------------------*/
    public Tablero getTablerin(){
        return this.tablerin;
    }

/*-----------------------------------------------------------------------
	getTableroDisparo
	Entradas: No posee
    Salidas: Un objeto de tipo tablero
    Restricciones: No posee
	Funcionamiento: 
        - Retorna un objeto de tipo tablero
-----------------------------------------------------------------------*/
    public Tablero getTableroDisparo(){

        return this.tableroDisparo;
    }

/*-----------------------------------------------------------------------
	getCoordenadasBarcos
	Entradas: No posee
    Salidas: Un arrayList
    Restricciones: No posee
	Funcionamiento: 
        - Retorna un arrayList
-----------------------------------------------------------------------*/
    public ArrayList<Coordenada> getCoordenadasBarcos(){

        return coordenadasBarcos;

    }

/*-----------------------------------------------------------------------
	getBarcosHundidos
	Entradas: No posee
    Salidas: Un arrayList
    Restricciones: No posee
	Funcionamiento: 
        - Retorna un arrayList
-----------------------------------------------------------------------*/
    public ArrayList<Coordenada> getBarcosHundidos(){

        return barcosHundidos;
    }

/*-----------------------------------------------------------------------
	setBarcosHundidos
	Entradas: un objeto de tipo Coordenada
    Salidas: No posee
    Restricciones: debe ser un objeto de tipo coordenada
	Funcionamiento: 
        - Agrega una coordenada al arrayList
-----------------------------------------------------------------------*/
    public void setBarcosHundidos(Coordenada coordenada){

        barcosHundidos.add(coordenada);

    }

/*-----------------------------------------------------------------------
	iniciarJugador
	Entradas: un objeto de tipo jugador
    Salidas: No posee
    Restricciones: debe ser un objeto de tipo jugador
	Funcionamiento: 
        - Pide las posiciones deseadas por el jugador para los Barcos
        - Es un ciclo que no se detiene hasta tener todos los Barcos correspondientes al jugador
        - llama a las verificaciones del espacio en la matriz
        - crea los barcos con sus respectivas coordenadas, y los guarda en el jugador
        - imprime el oceano cuando se inserta correctamente un barco
-----------------------------------------------------------------------*/
    public void iniciarJugador(Jugador jugador){

        Scanner sn = new Scanner(System.in);


        System.out.println();
        System.out.println("Introduzca sus barcos");
        System.out.println("Tome en cuenta que tiene a su disposición una armada de un portaaviones, tres submarinos, tres destructores y dos fragatas");
        
        for(int ind = 1;ind <= 2;){
            if(jugador.tamañoLista() <= 2 ){
                Barco fragata = new Barco("fragata",1);
                System.out.println("Elija posicionamiento para su " + ind + " fragata");
    
                System.out.println("Introduzca un x para desplegar su barco:");
                int x = sn.nextInt();

                System.out.println("Introduzca un y para desplegar su barco:");
                int y = sn.nextInt();
    
                Coordenada coordenada = new Coordenada(x,y);

                if(tablerin.verificaEspacios(x, y) == true){
                    tablerin.despliegaBarcos(x,y);
                    fragata.insertarCoordenada(coordenada);
                    coordenadasBarcos.add(coordenada);
                    jugador.insertarBarco(fragata);
                    tablerin.imprimirOceano();
                    ind++;
                }
            }
        }
            
        for(int ind = 1;ind <= 3;){
            if(jugador.tamañoLista() <= 5 ){
                Barco destructor = new Barco("destructor",2);
                System.out.println("Elija posicionamiento inicial para su " + ind + " destructor");

                System.out.println("Introduzca un x para desplegar su barco:");
                int x = sn.nextInt();

                System.out.println("Introduzca un y para desplegar su barco:");
                int y = sn.nextInt();

                Coordenada coordenada = new Coordenada(x,y);

                System.out.println("¿Como desea la orientación de su Barco?");
                System.out.println("1. Horizontal");
                System.out.println("2. Vertical");
                int orientacion = sn.nextInt();

                if(orientacion == 1){
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Izquierda");
                    System.out.println("2. Derecha");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x-1, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x-1,y);
                            Coordenada coordenada1 = new Coordenada(x-1,y);
                            destructor.insertarCoordenada(coordenada);
                            destructor.insertarCoordenada(coordenada1);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            jugador.insertarBarco(destructor);
                            tablerin.imprimirOceano();
                            ind++;
                        }
                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x+1, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x+1,y);
                            Coordenada coordenada1 = new Coordenada(x+1,y);
                            destructor.insertarCoordenada(coordenada);
                            destructor.insertarCoordenada(coordenada1);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            jugador.insertarBarco(destructor);
                            tablerin.imprimirOceano();
                            ind++;
                        }
                    }
                }else{
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Abajo");
                    System.out.println("2. Arriba");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y+1) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y+1);
                            Coordenada coordenada1 = new Coordenada(x,y+1);
                            destructor.insertarCoordenada(coordenada);
                            destructor.insertarCoordenada(coordenada1);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            jugador.insertarBarco(destructor);
                            tablerin.imprimirOceano();
                            ind++;
                        }
                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y-1) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y-1);
                            Coordenada coordenada1 = new Coordenada(x,y-1);
                            destructor.insertarCoordenada(coordenada);
                            destructor.insertarCoordenada(coordenada1);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            jugador.insertarBarco(destructor);
                            tablerin.imprimirOceano();
                            ind++;
                        }
                    }
                }
            }
        }

        for(int ind = 1;ind <= 3;){
            if(jugador.tamañoLista() <= 8 ){
                Barco submarino = new Barco("submarino",3);
                System.out.println("Elija posicionamiento inicial para su " + ind + " submarino");

                System.out.println("Introduzca un x para desplegar su barco:");
                int x = sn.nextInt();

                System.out.println("Introduzca un y para desplegar su barco:");
                int y = sn.nextInt();

                Coordenada coordenada = new Coordenada(x,y);

                System.out.println("¿Como desea la orientación de su Barco?");
                System.out.println("1. Horizontal");
                System.out.println("2. Vertical");
                int orientacion = sn.nextInt();

                if(orientacion == 1){
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Izquierda");
                    System.out.println("2. Derecha");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x-1, y) == true && tablerin.verificaEspacios(x-2, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x-1,y);
                            tablerin.despliegaBarcos(x-2,y);
                            Coordenada coordenada1 = new Coordenada(x-1,y);
                            Coordenada coordenada2 = new Coordenada(x-2,y);
                            submarino.insertarCoordenada(coordenada);
                            submarino.insertarCoordenada(coordenada1);
                            submarino.insertarCoordenada(coordenada2);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            jugador.insertarBarco(submarino);
                            tablerin.imprimirOceano();
                            ind++;
                        }

                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x+1, y) == true && tablerin.verificaEspacios(x+2, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x+1,y);
                            tablerin.despliegaBarcos(x+2,y);
                            Coordenada coordenada1 = new Coordenada(x+1,y);
                            Coordenada coordenada2 = new Coordenada(x+2,y);
                            submarino.insertarCoordenada(coordenada);
                            submarino.insertarCoordenada(coordenada1);
                            submarino.insertarCoordenada(coordenada2);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            jugador.insertarBarco(submarino);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                    }
                }else{
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Abajo");
                    System.out.println("2. Arriba");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y+1) == true && tablerin.verificaEspacios(x, y+2) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y+1);
                            tablerin.despliegaBarcos(x,y+2);
                            Coordenada coordenada1 = new Coordenada(x,y+1);
                            Coordenada coordenada2 = new Coordenada(x,y+2);
                            submarino.insertarCoordenada(coordenada);
                            submarino.insertarCoordenada(coordenada1);
                            submarino.insertarCoordenada(coordenada2);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            jugador.insertarBarco(submarino);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }

                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y-1) == true && tablerin.verificaEspacios(x, y-2) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y-1);
                            tablerin.despliegaBarcos(x,y-2);
                            Coordenada coordenada1 = new Coordenada(x,y-1);
                            Coordenada coordenada2 = new Coordenada(x,y-2);
                            submarino.insertarCoordenada(coordenada);
                            submarino.insertarCoordenada(coordenada1);
                            submarino.insertarCoordenada(coordenada2);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            jugador.insertarBarco(submarino);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                    }
                }
            }
        }
        for(int ind = 1;ind <= 1;){
            if(jugador.tamañoLista() <= 9 ){
                Barco portaaviones = new Barco("portaaviones",4);
                System.out.println("Elija posicionamiento inicial para su " + ind + " portaaviones");

                System.out.println("Introduzca un x para desplegar su barco:");
                int x = sn.nextInt();

                System.out.println("Introduzca un y para desplegar su barco:");
                int y = sn.nextInt();

                Coordenada coordenada = new Coordenada(x,y);

                System.out.println("¿Como desea la orientación de su Barco?");
                System.out.println("1. Horizontal");
                System.out.println("2. Vertical");
                int orientacion = sn.nextInt();

                if(orientacion == 1){
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Izquierda");
                    System.out.println("2. Derecha");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x-1, y) == true && tablerin.verificaEspacios(x-2, y) == true && tablerin.verificaEspacios(x-3, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x-1,y);
                            tablerin.despliegaBarcos(x-2,y);
                            tablerin.despliegaBarcos(x-3,y);
                            Coordenada coordenada1 = new Coordenada(x-1,y);
                            Coordenada coordenada2 = new Coordenada(x-2,y);
                            Coordenada coordenada3 = new Coordenada(x-3,y);
                            portaaviones.insertarCoordenada(coordenada);
                            portaaviones.insertarCoordenada(coordenada1);
                            portaaviones.insertarCoordenada(coordenada2);
                            portaaviones.insertarCoordenada(coordenada3);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            coordenadasBarcos.add(coordenada3);
                            jugador.insertarBarco(portaaviones);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                        
                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x+1, y) == true && tablerin.verificaEspacios(x+2, y) == true && tablerin.verificaEspacios(x+3, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x+1,y);
                            tablerin.despliegaBarcos(x+2,y);
                            tablerin.despliegaBarcos(x+3,y);
                            Coordenada coordenada1 = new Coordenada(x+1,y);
                            Coordenada coordenada2 = new Coordenada(x+2,y);
                            Coordenada coordenada3 = new Coordenada(x+3,y);
                            portaaviones.insertarCoordenada(coordenada);
                            portaaviones.insertarCoordenada(coordenada1);
                            portaaviones.insertarCoordenada(coordenada2);
                            portaaviones.insertarCoordenada(coordenada3);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            coordenadasBarcos.add(coordenada3);
                            jugador.insertarBarco(portaaviones);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                    }
                }else{
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Abajo");
                    System.out.println("2. Arriba");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y+1) == true && tablerin.verificaEspacios(x, y+2) == true && tablerin.verificaEspacios(x, y+3) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y+1);
                            tablerin.despliegaBarcos(x,y+2);
                            tablerin.despliegaBarcos(x,y+3);
                            Coordenada coordenada1 = new Coordenada(x,y+1);
                            Coordenada coordenada2 = new Coordenada(x,y+2);
                            Coordenada coordenada3 = new Coordenada(x,y+3);
                            portaaviones.insertarCoordenada(coordenada);
                            portaaviones.insertarCoordenada(coordenada1);
                            portaaviones.insertarCoordenada(coordenada2);
                            portaaviones.insertarCoordenada(coordenada3);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            coordenadasBarcos.add(coordenada3);
                            jugador.insertarBarco(portaaviones);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }

                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y-1) == true && tablerin.verificaEspacios(x, y-2) == true && tablerin.verificaEspacios(x, y-3) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y-1);
                            tablerin.despliegaBarcos(x,y-2);
                            tablerin.despliegaBarcos(x,y-3);
                            Coordenada coordenada1 = new Coordenada(x,y-1);
                            Coordenada coordenada2 = new Coordenada(x,y-2);
                            Coordenada coordenada3 = new Coordenada(x,y-3);
                            portaaviones.insertarCoordenada(coordenada);
                            portaaviones.insertarCoordenada(coordenada1);
                            portaaviones.insertarCoordenada(coordenada2);
                            portaaviones.insertarCoordenada(coordenada3);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            coordenadasBarcos.add(coordenada3);
                            jugador.insertarBarco(portaaviones);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                    }
                }
            }
        }

    }
}
