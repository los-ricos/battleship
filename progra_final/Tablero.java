/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

import java.util.Scanner;



public class Tablero{
    private int lineas;
    private int columnas;
    private String[][] oceano;


/*-----------------------------------------------------------------------
	Tablero
	Entradas: No recibe parámetros
    Salidas: No posee salidas
    Restricciones: No posee
	Funcionamiento: 
        - Constructor vacio con valores por defecto
-----------------------------------------------------------------------*/
    public Tablero(){
        lineas = 10;
        columnas = 10;
        oceano = new String[lineas][columnas];
    }
    
/*-----------------------------------------------------------------------
	iniciarOceano
	Entradas: No recibe parámetros
    Salidas: No posee salidas
    Restricciones: No posee
	Funcionamiento: 
        - Mediante un ciclo imprime en pantalla un arte representando un oceano vacio
        - Inicializa una matriz llena de espacios en blanco
-----------------------------------------------------------------------*/

    public void iniciarOceano(){

        //primera fila de numeros
         System.out.print("  ");
            for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
                System.out.println();
            
            //cuerpo de oceano
            for(int i = 0; i < oceano.length; i++) {
                for (int j = 0; j < oceano[i].length; j++) {
                    //oceano[i][j] = 0;
                    oceano[i][j] = " ";
                    if (j == 0)
                        System.out.print(i + "|" + oceano[i][j] + " ");
                    else if(j == oceano[i].length - 1)
                        System.out.print(oceano[i][j] + "|" + i);
                            else
                        System.out.print(oceano[i][j] + " ");
                }
                System.out.println();
            }
            

        //ultima fila de numeros
        System.out.print("  ");
        for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
        System.out.println();
    }


    public void imprimeGolpe(int i, int j){

        oceano[i][j]= "@";


    }
    public void imprimeAfuera(int i, int j){

        oceano[i][j]= "A";
    }
    public void BarcoDown(int i, int j){

        oceano[i][j]= "X";

        
    }

/*-----------------------------------------------------------------------
	imprimirOceano
	Entradas: No recibe parámetros
    Salidas: No posee salidas
    Restricciones: No posee
	Funcionamiento: 
        - Mediante un ciclo imprime en pantalla un arte representando un oceano con sus respectivos cambios
        - No modifica la matriz solo imprime lo que tenga almacenado
-----------------------------------------------------------------------*/
    public void imprimirOceano(){

        //primera fila de numeros
         System.out.print("  ");
            for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
                System.out.println();
            
            //cuerpo de oceano
            for(int i = 0; i < oceano.length; i++) {
                for (int j = 0; j < oceano[i].length; j++) {
                    if (j == 0)
                        System.out.print(i + "|" + oceano[i][j] + " ");
                    else if(j == oceano[i].length - 1)
                        System.out.print(oceano[i][j] + "|" + i);
                            else
                        System.out.print(oceano[i][j] + " ");
                }
                System.out.println();
            }
            
        //ultima fila de numeros
        System.out.print("  ");
        for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
        System.out.println();
    }

/*-----------------------------------------------------------------------
	despliegaBarcos
	Entradas: Recibe dos numeros enteros
    Salidas: No posee salidas
    Restricciones: Deben ser numeros enteros
	Funcionamiento: 
        - Valida si la posicion solicitada está habilitada
        - Imprime mensajes de error si no se colocan los barcos
        - Modifica la matriz en una posicion especifica
-----------------------------------------------------------------------*/
    public void despliegaBarcos(int y, int x){
            if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && (oceano[x][y] == " "))
            {
                oceano[x][y] =  "#";
            }else if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && oceano[x][y] == "#"){
                System.out.println("No se pueden colocar dos barcos en la misma ubicación");
            }else if((x < 0 || x >= lineas) || (y < 0 || y >= columnas)){
                System.out.println("Llegaste a los limites de " + lineas + " por " + columnas + " del reino de Poseidon");}
    }
/*-----------------------------------------------------------------------
	despliegaMisil
	Entradas: Recibe 2 números enteros
    Salidas: Un número entero (1 o 2 o 3 o 4 o 5)
    Restricciones: Deben ser números enteros
	Funcionamiento: 
        - Valida si la posicion solicitada está habilitada
        - Imprime mensajes de error si no se colocan los misiles
        - Modifica la matriz en una posicion especifica
-----------------------------------------------------------------------*/
    public int despliegaMisil(int y, int x){
        if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && (oceano[x][y] == " "))
        {
            return 1;
        }else if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && oceano[x][y] == "#"){
            return 2;
        
        }else if((x < 0 || x >= lineas) || (y < 0 || y >= columnas)){
            System.out.println("Llegaste a los limites de " + lineas + " por " + columnas + " del reino de Poseidon");
            return 3;
        
        }else if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && (oceano[x][y] == "@" || oceano[x][y]=="X")){
            return 4;
        
        }
        else{

            return 5;

        } 
    
    }

/*-----------------------------------------------------------------------
	verificaEspacios
	Entradas: Recibe dos numeros enteros
    Salidas: Retorna true si está disponible o false
    Restricciones: Deben ser numeros enteros
	Funcionamiento: 
        - Valida si la posicion solicitada está habilitada
        - Imprime mensajes de error si no está habilitada la posicion
-----------------------------------------------------------------------*/
    public Boolean verificaEspacios(int y,int x){
        if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && (oceano[x][y] == " ")){
            return true;
        }else if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && oceano[x][y] == "#"){
            System.out.println("No se pueden colocar dos barcos en la misma ubicación, intente de nuevo");
            return false;
        }else if((x < 0 || x >= lineas) || (y < 0 || y >= columnas)){
            System.out.println("Llegaste a los limites de " + lineas + " por " + columnas + " de los increíbles dominios de Poseidon");
            return false;
        }else{
            System.out.println("Poseidon no encuentra este error");
            return false;
        }
    }
}

