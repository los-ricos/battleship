import java.util.Scanner;

public class Tablero{
    private int lineas;
    private int columnas;
    private String[][] oceano;
    private int tamannoBarco;

    public Tablero(){
        lineas = 10;
        columnas = 10;
        oceano = new String[lineas][columnas];
    }

    public void imprimirOceano(){

        //primera fila de numeros
         System.out.print("  ");
            for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
                System.out.println();
            
            //cuerpo de oceano
            for(int i = 0; i < oceano.length; i++) {
                for (int j = 0; j < oceano[i].length; j++) {
                    //oceano[i][j] = 0;
                    oceano[i][j] = " ";
                    if (j == 0)
                        System.out.print(i + "|" + oceano[i][j] + "~");
                    else if(j == oceano[i].length - 1)
                        System.out.print(oceano[i][j] + "|" + i);
                            else
                        System.out.print(oceano[i][j] + "~");
                }
                System.out.println();
            }
            

        //ultima fila de numeros
        System.out.print("  ");
        for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
        System.out.println();

    }

    public void addBarco(int i,int j, int tamanno, boolean orientacion, boolean cola){

        if(tamanno == 1){
            if(oceano[i][j] != " "){ // Es fragata
                System.out.println("Campo ocupado");

            }else{
                this.oceano[i][j] = "#";
            }
        }else if(tamanno == 2){ // Es destructor

            if(oceano[i][j] != " "){
                System.out.println("Campo ocupado");

            }else{
                if(orientacion == true){ // Vertical

                    if(cola == false){ // Hacia arriba, i-1

                        if(i == 0){ // Significa que está en el borde de arriba
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i-1][j] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está ak7, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i-1][j] = "#";
                            }

                        }

                    }else{ // cola == True, hacia abajo, i+1

                        if(i == 9){ // Significa que está en el borde de abajo
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i+1][j] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i+1][j] = "#";
                            }

                        }

                    }
                }else{ // Horizontal

                    if(cola == false){ // Hacia la derecha, j+1
                        if(j == 9){ // Significa que está al borde derecho
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i][j+1] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i][j+1] = "#";
                            }
                        }

                    }else{ // cola == True, Hacia la izquierda, j-1

                        if(j == 0){ // Significa que está al borde izquierdo
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i][j-1] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i][j-1] = "#";
                            }

                        }

                    }
                }
            }
        }else if(tamanno == 3){ // Es submarino
            if(oceano[i][j] != " "){
                System.out.println("Campo ocupado");
            }else{
                if(orientacion == true){ // Vertical

                    if(cola == false){ // Hacia arriba
                        if(i == 0 || i == 1){ // Pregunta si está en las filas 0 o 1
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i-1][j] != " " || oceano[i-2][j] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i-1][j] = "#";
                                this.oceano[i-2][j] = "#";
                            }

                        }

                    }else{ // Hacia abajo
                        if(i == 8 || i == 9){ // Pregunta si está en filas 8 o 9
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i+1][j] != " " || oceano[i+2][j] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i+1][j] = "#";
                                this.oceano[i+2][j] = "#";
                            }

                        }

                    }
                }else{ // Horizontal
                    if(cola == false){ // Hacia la derecha
                        if(j == 8 || j == 9){ // Pregunta si está en las columnas 8 o 9
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i][j+1] != " " || oceano[i][j+2] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i][j+1] = "#";
                                this.oceano[i][j+2] = "#";
    
                            }
                        }

                    }else{ // Hacia la izquierda
                        if(j == 0 || j == 1){ // Pregunta si está en las columnas 0 o 1
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i][j-1] != " " || oceano[i][j-2] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i][j-1] = "#";
                                this.oceano[i][j-2] = "#";

                            }
                        }

                    }
                }
            }
        }else{ // Es portaaviones
            if(oceano[i][j] != " "){
                System.out.println("Campo ocupado");
            }else{
                if(orientacion == true){ // Vertical
                    if(cola == false){ // Hacia arriba

                        if(i == 0 || i == 1 || i == 2){ // Pregunta por las 3 primeras filas
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i-1][j] != " " || oceano[i-2][j] != " " || oceano[i-3][j] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i-1][j] = "#";
                                this.oceano[i-2][j] = "#";
                                this.oceano[i-3][j] = "#";
                            }


                        }

                    }else{ // Hacia abajo
                        if(i == 7 || i == 8 || i == 9){ // Pregunta por las últimas 3 filas
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i+1][j] != " " || oceano[i+2][j] != " " || oceano[i+3][j] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i+1][j] = "#";
                                this.oceano[i+2][j] = "#";
                                this.oceano[i+3][j] = "#";
                            }
                        }

                    }

                }else{ // Horizontal

                    if(cola == false){ // Hacia la derecha
                        if(j == 7 || j == 8 || j == 9){ // Pregunta por las últimas 3 columnas
                            System.out.println("Posición inválida");
                            
                        }else{
                            if(oceano[i][j+1] != " " || oceano[i][j+2] != " " || oceano[i][j+3] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i][j+1] = "#";
                                this.oceano[i][j+2] = "#";
                                this.oceano[i][j+3] = "#";
                            }
                        }


                    }else{ // Hacia la izquierda
                        if(j == 0 || j == 1 || j == 2){ // Pregunta por las primeras 3 columnas
                            System.out.println("Posición inválida");

                        }else{
                            if(oceano[i][j-1] != " " || oceano[i][j-2] != " " || oceano[i][j-3] != " "){ // Valida si hay barcos
                                System.out.println("Hay un barco en esa posición, intente de nuevo");

                            }else{ // Si todo está bien, pone el barco
                                this.oceano[i][j] = "#";
                                this.oceano[i][j-1] = "#";
                                this.oceano[i][j-2] = "#";
                                this.oceano[i][j-3] = "#";
                            }
                        }
                    }
                }
            }
        }
    }
}

