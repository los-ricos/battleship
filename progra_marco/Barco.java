import java.util.ArrayList;
public class Barco{
    private String nombre;
    //true = vertical y false = horizontal
    private boolean orientacion;
    private int tamanno;
    ArrayList<Coordenada> coordenadasBarco = new ArrayList<Coordenada>();


    public Barco(){
        nombre = " ";
        orientacion = false;
        tamanno = 0;

    }

    public Barco(String nombre,int tamanno){
        this.nombre = nombre;
        this.tamanno = tamanno;
    }

    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public boolean getOrientacion(){
        return this.orientacion;

    }
    public void setOrientacion(boolean orientacion){
        this.orientacion=orientacion;
    }

    public int getTamanno(){
        return tamanno;
    }

    public void setTamanno(int tamanno){
        this.tamanno = tamanno;
    }

    public void insertarCoordenada(Coordenada coordenada){
        coordenadasBarco.add(coordenada);
    }

    public void eliminarCoordenada(Coordenada coordenada){
        coordenadasBarco.remove(coordenada);;
    }
    
    public Object obtenerCoordenada(int pos){
      return coordenadasBarco.get(pos);
    }

    public int obtenerX(int pos){
        Coordenada coordenada = coordenadasBarco.get(pos);
        return coordenada.getX();
      }

    public int obtenerY(int pos){
        Coordenada coordenada = coordenadasBarco.get(pos);
        return coordenada.getY();
      }
    
    public boolean esVacia(){
        return coordenadasBarco.isEmpty();  
    }
    
    public int tamañoLista(){
      return coordenadasBarco.size();
    }
}