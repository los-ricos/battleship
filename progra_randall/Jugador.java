import java.util.ArrayList;
import java.util.Scanner;

public class Jugador{
    private String nombre;
    private int clave;
    private Tablero tablerin = new Tablero();
    private Tablero tableroDisparo = new Tablero();
    private ArrayList<Coordenada> coordenadasBarcos = new ArrayList<Coordenada>();
    private ArrayList<Coordenada> barcosHundidos=new ArrayList<Coordenada>();
    private ArrayList<Barco> barcosVivos = new ArrayList<Barco>();
    


    public Jugador(){
        nombre = "";
        clave = 0;
    }
    
    public Jugador(int clave, String nombre){
        this.clave = clave;
        this.nombre = nombre;

    }
    
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public int getClave() {
        return this.clave;
    }
    
    public void setClave(int clave){
        this.clave = clave;
    }

    public void insertarBarco(Barco barco){
        barcosVivos.add(barco);
    }

    public void eliminarBarco(Barco barco){
        barcosVivos.remove(barco);
    }
    public Barco obtenerBarco(int pos){ // Este tipo de retorno era de tipo Object, por qué? Pregunta para Jeremy
      return barcosVivos.get(pos);
    }
    
    public boolean esVacia(){
        return barcosVivos.isEmpty();
    }
    
    public int tamañoLista(){
      return barcosVivos.size();
    }

    public Tablero getTablerin(){
        return this.tablerin;
    }

    public Tablero getTableroDisparo(){

        return this.tableroDisparo;


    }
    public ArrayList<Coordenada> getCoordenadasBarcos(){

        return coordenadasBarcos;

    }
    public ArrayList<Coordenada> getBarcosHundidos(){

        return barcosHundidos;
    }
    public void setBarcosHundidos(Coordenada coordenada){

        barcosHundidos.add(coordenada);

    }

    public void iniciarJugador(Jugador jugador){

        Scanner sn = new Scanner(System.in);


        System.out.println();
        System.out.println("Introduzca sus barcos");
        System.out.println("Tome en cuenta que tiene a su disposición una armada de un portaaviones, tres submarinos, tres destructores y dos fragatas");
        
        for(int ind = 1;ind <= 2;){
            if(jugador.tamañoLista() <= 2 ){
                Barco fragata = new Barco("fragata",1);
                System.out.println("Elija posicionamiento para su " + ind + " fragata");
    
                System.out.println("Introduzca un x para desplegar su barco:");
                int x = sn.nextInt();

                System.out.println("Introduzca un y para desplegar su barco:");
                int y = sn.nextInt();
    
                Coordenada coordenada = new Coordenada(x,y);

                if(tablerin.verificaEspacios(x, y) == true){
                    tablerin.despliegaBarcos(x,y);
                    fragata.insertarCoordenada(coordenada);
                    coordenadasBarcos.add(coordenada);
                    jugador.insertarBarco(fragata);
                    tablerin.imprimirOceano();
                    ind++;
                }
            }
        }
            
        for(int ind = 1;ind <= 3;){
            if(jugador.tamañoLista() <= 5 ){
                Barco destructor = new Barco("destructor",2);
                System.out.println("Elija posicionamiento inicial para su " + ind + " destructor");

                System.out.println("Introduzca un x para desplegar su barco:");
                int x = sn.nextInt();

                System.out.println("Introduzca un y para desplegar su barco:");
                int y = sn.nextInt();

                Coordenada coordenada = new Coordenada(x,y);

                System.out.println("¿Como desea la orientación de su Barco?");
                System.out.println("1. Horizontal");
                System.out.println("2. Vertical");
                int orientacion = sn.nextInt();

                if(orientacion == 1){
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Izquierda");
                    System.out.println("2. Derecha");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x-1, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x-1,y);
                            Coordenada coordenada1 = new Coordenada(x-1,y);
                            destructor.insertarCoordenada(coordenada);
                            destructor.insertarCoordenada(coordenada1);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            jugador.insertarBarco(destructor);
                            tablerin.imprimirOceano();
                            ind++;
                        }
                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x+1, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x+1,y);
                            Coordenada coordenada1 = new Coordenada(x+1,y);
                            destructor.insertarCoordenada(coordenada);
                            destructor.insertarCoordenada(coordenada1);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            jugador.insertarBarco(destructor);
                            tablerin.imprimirOceano();
                            ind++;
                        }
                    }
                }else{
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Abajo");
                    System.out.println("2. Arriba");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y+1) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y+1);
                            Coordenada coordenada1 = new Coordenada(x,y+1);
                            destructor.insertarCoordenada(coordenada);
                            destructor.insertarCoordenada(coordenada1);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            jugador.insertarBarco(destructor);
                            tablerin.imprimirOceano();
                            ind++;
                        }
                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y-1) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y-1);
                            Coordenada coordenada1 = new Coordenada(x,y-1);
                            destructor.insertarCoordenada(coordenada);
                            destructor.insertarCoordenada(coordenada1);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            jugador.insertarBarco(destructor);
                            tablerin.imprimirOceano();
                            ind++;
                        }
                    }
                }
            }
        }

        for(int ind = 1;ind <= 3;){
            if(jugador.tamañoLista() <= 8 ){
                Barco submarino = new Barco("submarino",3);
                System.out.println("Elija posicionamiento inicial para su " + ind + " submarino");

                System.out.println("Introduzca un x para desplegar su barco:");
                int x = sn.nextInt();

                System.out.println("Introduzca un y para desplegar su barco:");
                int y = sn.nextInt();

                Coordenada coordenada = new Coordenada(x,y);

                System.out.println("¿Como desea la orientación de su Barco?");
                System.out.println("1. Horizontal");
                System.out.println("2. Vertical");
                int orientacion = sn.nextInt();

                if(orientacion == 1){
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Izquierda");
                    System.out.println("2. Derecha");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x-1, y) == true && tablerin.verificaEspacios(x-2, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x-1,y);
                            tablerin.despliegaBarcos(x-2,y);
                            Coordenada coordenada1 = new Coordenada(x-1,y);
                            Coordenada coordenada2 = new Coordenada(x-2,y);
                            submarino.insertarCoordenada(coordenada);
                            submarino.insertarCoordenada(coordenada1);
                            submarino.insertarCoordenada(coordenada2);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            jugador.insertarBarco(submarino);
                            tablerin.imprimirOceano();
                            ind++;
                        }

                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x+1, y) == true && tablerin.verificaEspacios(x+2, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x+1,y);
                            tablerin.despliegaBarcos(x+2,y);
                            Coordenada coordenada1 = new Coordenada(x+1,y);
                            Coordenada coordenada2 = new Coordenada(x+2,y);
                            submarino.insertarCoordenada(coordenada);
                            submarino.insertarCoordenada(coordenada1);
                            submarino.insertarCoordenada(coordenada2);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            jugador.insertarBarco(submarino);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                    }
                }else{
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Abajo");
                    System.out.println("2. Arriba");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y+1) == true && tablerin.verificaEspacios(x, y+2) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y+1);
                            tablerin.despliegaBarcos(x,y+2);
                            Coordenada coordenada1 = new Coordenada(x,y+1);
                            Coordenada coordenada2 = new Coordenada(x,y+2);
                            submarino.insertarCoordenada(coordenada);
                            submarino.insertarCoordenada(coordenada1);
                            submarino.insertarCoordenada(coordenada2);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            jugador.insertarBarco(submarino);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }

                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y-1) == true && tablerin.verificaEspacios(x, y-2) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y-1);
                            tablerin.despliegaBarcos(x,y-2);
                            Coordenada coordenada1 = new Coordenada(x,y-1);
                            Coordenada coordenada2 = new Coordenada(x,y-2);
                            submarino.insertarCoordenada(coordenada);
                            submarino.insertarCoordenada(coordenada1);
                            submarino.insertarCoordenada(coordenada2);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            jugador.insertarBarco(submarino);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                    }
                }
            }
        }
        for(int ind = 1;ind <= 1;){
            if(jugador.tamañoLista() <= 9 ){
                Barco portaaviones = new Barco("portaaviones",4);
                System.out.println("Elija posicionamiento inicial para su " + ind + " portaaviones");

                System.out.println("Introduzca un x para desplegar su barco:");
                int x = sn.nextInt();

                System.out.println("Introduzca un y para desplegar su barco:");
                int y = sn.nextInt();

                Coordenada coordenada = new Coordenada(x,y);

                System.out.println("¿Como desea la orientación de su Barco?");
                System.out.println("1. Horizontal");
                System.out.println("2. Vertical");
                int orientacion = sn.nextInt();

                if(orientacion == 1){
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Izquierda");
                    System.out.println("2. Derecha");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x-1, y) == true && tablerin.verificaEspacios(x-2, y) == true && tablerin.verificaEspacios(x-3, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x-1,y);
                            tablerin.despliegaBarcos(x-2,y);
                            tablerin.despliegaBarcos(x-3,y);
                            Coordenada coordenada1 = new Coordenada(x-1,y);
                            Coordenada coordenada2 = new Coordenada(x-2,y);
                            Coordenada coordenada3 = new Coordenada(x-3,y);
                            portaaviones.insertarCoordenada(coordenada);
                            portaaviones.insertarCoordenada(coordenada1);
                            portaaviones.insertarCoordenada(coordenada2);
                            portaaviones.insertarCoordenada(coordenada3);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            coordenadasBarcos.add(coordenada3);
                            jugador.insertarBarco(portaaviones);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                        
                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x+1, y) == true && tablerin.verificaEspacios(x+2, y) == true && tablerin.verificaEspacios(x+3, y) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x+1,y);
                            tablerin.despliegaBarcos(x+2,y);
                            tablerin.despliegaBarcos(x+3,y);
                            Coordenada coordenada1 = new Coordenada(x+1,y);
                            Coordenada coordenada2 = new Coordenada(x+2,y);
                            Coordenada coordenada3 = new Coordenada(x+3,y);
                            portaaviones.insertarCoordenada(coordenada);
                            portaaviones.insertarCoordenada(coordenada1);
                            portaaviones.insertarCoordenada(coordenada2);
                            portaaviones.insertarCoordenada(coordenada3);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            coordenadasBarcos.add(coordenada3);
                            jugador.insertarBarco(portaaviones);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                    }
                }else{
                    System.out.println("¿Para donde desea que esté dirigida la popa de su barco?");
                    System.out.println("1. Abajo");
                    System.out.println("2. Arriba");
                    int cola = sn.nextInt();

                    if(cola == 1){
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y+1) == true && tablerin.verificaEspacios(x, y+2) == true && tablerin.verificaEspacios(x, y+3) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y+1);
                            tablerin.despliegaBarcos(x,y+2);
                            tablerin.despliegaBarcos(x,y+3);
                            Coordenada coordenada1 = new Coordenada(x,y+1);
                            Coordenada coordenada2 = new Coordenada(x,y+2);
                            Coordenada coordenada3 = new Coordenada(x,y+3);
                            portaaviones.insertarCoordenada(coordenada);
                            portaaviones.insertarCoordenada(coordenada1);
                            portaaviones.insertarCoordenada(coordenada2);
                            portaaviones.insertarCoordenada(coordenada3);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            coordenadasBarcos.add(coordenada3);
                            jugador.insertarBarco(portaaviones);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }

                    }else{
                        if(tablerin.verificaEspacios(x, y) == true && tablerin.verificaEspacios(x, y-1) == true && tablerin.verificaEspacios(x, y-2) == true && tablerin.verificaEspacios(x, y-3) == true){
                            tablerin.despliegaBarcos(x,y);
                            tablerin.despliegaBarcos(x,y-1);
                            tablerin.despliegaBarcos(x,y-2);
                            tablerin.despliegaBarcos(x,y-3);
                            Coordenada coordenada1 = new Coordenada(x,y-1);
                            Coordenada coordenada2 = new Coordenada(x,y-2);
                            Coordenada coordenada3 = new Coordenada(x,y-3);
                            portaaviones.insertarCoordenada(coordenada);
                            portaaviones.insertarCoordenada(coordenada1);
                            portaaviones.insertarCoordenada(coordenada2);
                            portaaviones.insertarCoordenada(coordenada3);
                            coordenadasBarcos.add(coordenada);
                            coordenadasBarcos.add(coordenada1);
                            coordenadasBarcos.add(coordenada2);
                            coordenadasBarcos.add(coordenada3);
                            jugador.insertarBarco(portaaviones);
                            jugador.getTablerin().imprimirOceano();
                            ind++;
                        }
                    }
                }
            }
        }

    }
}
