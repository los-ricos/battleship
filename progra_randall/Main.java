import java.util.InputMismatchException;
import java.util.Scanner;

class Main {

    public static void main(String[] args) {
        
        String nombreIngresado;
        int claveIngresada;
        Scanner sn = new Scanner(System.in);
    try{
        System.out.println("Primer contendiente");
        System.out.println("¿Quién se atreve a combatir?");
        
        nombreIngresado = sn.next();
        
        System.out.println("Ingrese su contraseña númerica:");
        claveIngresada = sn.nextInt();

        
    
        Jugador usuario1 = new Jugador(claveIngresada, nombreIngresado);
        System.out.println("\nBienvenido a Battleship " + usuario1.getNombre());
        System.out.println("\nSu contraseña es " + usuario1.getClave() + " por favor recuerdela ya que la necesitará\n");
        System.out.println("\n A continuación se le presenta su oceano, por favor despliegue su flota\n");
        
        usuario1.getTablerin().iniciarOceano();
        usuario1.iniciarJugador(usuario1);
        usuario1.getTableroDisparo().iniciarOceano();
   

        System.out.println("\nIntroduzca un número\n");

        System.out.print("\033[H\033[2J");
        System.out.flush();

        System.out.println("Segundo contendiente");
        System.out.println("¿Quién se atreve a combatir?");
        nombreIngresado = sn.next();

        System.out.println("Ingrese su contraseña númerica:");
        claveIngresada = sn.nextInt();
        
        Jugador usuario2 = new Jugador(claveIngresada, nombreIngresado);
        System.out.println("Bienvenido a Battleship " + usuario2.getNombre());
        System.out.println("\nSu contraseña es " + usuario2.getClave() + " por favor recuerdela ya que la necesitará\n");
        System.out.println("\nA continuación se le presenta su oceano, por favor despliegue su flota\n");
        
        usuario2.getTablerin().iniciarOceano();
        usuario2.iniciarJugador(usuario2);
        usuario2.getTableroDisparo().iniciarOceano();
        //Para centrar la pantalla.
        System.out.print("\033[H\033[2J");
        System.out.flush();

        //En esta parte se realiza los disparos y se comienza a jugar.
        Jugada jugada=new Jugada(usuario1,usuario2);
        boolean jugando=true;
        while(jugando!=false){
        jugada.Jugar(usuario1, usuario2);
        }
    }catch(InputMismatchException e){

            System.out.println("\nIntroduzca un número\n");
        }
    


    }
}