import java.util.Scanner;

public class Tablero{
    private int lineas;
    private int columnas;
    private String[][] oceano;
    private int tamannoBarco;

    public Tablero(){
        lineas = 10;
        columnas = 10;
        oceano = new String[lineas][columnas];
    }
    
    public void iniciarOceano(){

        //primera fila de numeros
         System.out.print("  ");
            for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
                System.out.println();
            
            //cuerpo de oceano
            for(int i = 0; i < oceano.length; i++) {
                for (int j = 0; j < oceano[i].length; j++) {
                    //oceano[i][j] = 0;
                    oceano[i][j] = " ";
                    if (j == 0)
                        System.out.print(i + "|" + oceano[i][j] + " ");
                    else if(j == oceano[i].length - 1)
                        System.out.print(oceano[i][j] + "|" + i);
                            else
                        System.out.print(oceano[i][j] + " ");
                }
                System.out.println();
            }
            

        //ultima fila de numeros
        System.out.print("  ");
        for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
        System.out.println();
    }


    public void imprimeGolpe(int i, int j){

        oceano[i][j]= "@";


    }
    public void imprimeAfuera(int i, int j){

        oceano[i][j]= "A";
    }
    public void BarcoDown(int i, int j){

        oceano[i][j]= "X";

        
    }


    public void imprimirOceano(){

        //primera fila de numeros
         System.out.print("  ");
            for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
                System.out.println();
            
            //cuerpo de oceano
            for(int i = 0; i < oceano.length; i++) {
                for (int j = 0; j < oceano[i].length; j++) {
                    //oceano[i][j] = " ";
                    if (j == 0)
                        System.out.print(i + "|" + oceano[i][j] + " ");
                    else if(j == oceano[i].length - 1)
                        System.out.print(oceano[i][j] + "|" + i);
                            else
                        System.out.print(oceano[i][j] + " ");
                }
                System.out.println();
            }
            
        //ultima fila de numeros
        System.out.print("  ");
        for(int i = 0; i < columnas; i++)
                System.out.print(i + " ");
        System.out.println();
    }

    public void despliegaBarcos(int y, int x){
            if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && (oceano[x][y] == " "))
            {
                oceano[x][y] =  "#";
            }else if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && oceano[x][y] == "#"){
                System.out.println("No se pueden colocar dos barcos en la misma ubicación");
            }else if((x < 0 || x >= lineas) || (y < 0 || y >= columnas)){
                System.out.println("Llegaste a los limites de " + lineas + " por " + columnas + " del reino de Poseidon");}
    }

    //Validaciones cuando se despliega un misil.
    //Entradas: 2 números enteros que son las coordenadas "x" y "y".
    //Salidas: Un número entero
    //Restricciones: No tiene
    public int despliegaMisil(int y, int x){
        if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && (oceano[x][y] == " "))
        {
            return 1;
        }else if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && oceano[x][y] == "#"){
            return 2;
        
        }else if((x < 0 || x >= lineas) || (y < 0 || y >= columnas)){
            System.out.println("Llegaste a los limites de " + lineas + " por " + columnas + " del reino de Poseidon");
            return 3;
        
        }else if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && (oceano[x][y] == "@" || oceano[x][y]=="X")){
            return 4;
        
        }
        else{

            return 5;

        } 
    
    }

    public Boolean verificaEspacios(int y,int x){
        if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && (oceano[x][y] == " ")){
            return true;
        }else if((x >= 0 && x < lineas) && (y >= 0 && y < columnas) && oceano[x][y] == "#"){
            System.out.println("No se pueden colocar dos barcos en la misma ubicación, intente de nuevo");
            return false;
        }else if((x < 0 || x >= lineas) || (y < 0 || y >= columnas)){
            System.out.println("Llegaste a los limites de " + lineas + " por " + columnas + " de los increíbles dominios de Poseidon");
            return false;
        }else{
            System.out.println("Poseidon no encuentra este error");
            return false;
        }
    }
}

