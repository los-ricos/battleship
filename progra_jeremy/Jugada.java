/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

import java.util.Scanner;
import java.util.ArrayList;
import java.util.InputMismatchException;
public class Jugada {
    //Atributos de la clase Jugada
    private Jugador usuario1;
    private Jugador usuario2;
    private ArrayList<Integer> barquitos = new ArrayList<Integer>();
    private ArrayList<Integer> barquitos2= new ArrayList<Integer>(); 

    //Constructor de la clase jugada
    //Entradas: No tiene
    //Salidas: Se crea 2 jugadore
    //Restricciones: No tiene
    public Jugada(){
        this.usuario1=new Jugador();
        this.usuario2= new Jugador();
    }

    //Constructor con parámetros de la clase jugada
    //Entradas: 2 jugadores
    //Salidas: No tiene
    //Restricciones: No tiene
    public Jugada(Jugador usuario1,Jugador usuario2) {
        this.usuario1=usuario1;    
        this.usuario2 = usuario2;
    }

    //Este método realiza los turnos de cada jugador,validaciones de disparos,daños de barco y si ganó o no. 
    //Entradas: 2 jugadores
    //Salidas: Un menú para que el usuario lo manipule, imprime las actualizaciones del mapa cada vez que se dispara.
    //Restricciones: Donde se pide números el usuario solo puede ingresar números
    public void Jugar(Jugador usuario1,Jugador usuario2){
        //Para centrar la pantalla
        System.out.print("\033[H\033[2J");
        System.out.flush();
        int claveIngresada=0;
        while (claveIngresada != usuario1.getClave()){

            Scanner sn = new Scanner(System.in);
            try{
            System.out.println("\n Turno de "+usuario1.getNombre()+"\n");
            System.out.println("Ingrese su contraseña númerica:");
            claveIngresada = sn.nextInt();
            }catch(InputMismatchException e){

                System.out.println("\nIntroduzca un número\n");

            }
  
        }
        boolean turno1 = true;
        
        while (turno1 == true){
            Scanner sn = new Scanner(System.in);
            System.out.println("\n"+usuario1.getNombre() +"\n");
            System.out.println(" Es tu turno Capitán(a): ");
            System.out.println("¿Adónde disparamos? ");
            usuario1.getTablerin().imprimirOceano();
            System.out.println("\n");
            System.out.println("ZONA DE DISPAROS");
            usuario1.getTableroDisparo().imprimirOceano();
            try{
            System.out.println("Introduzca un x para desplegar su Misil:");
            int y = sn.nextInt();

            System.out.println("Introduzca un y para desplegar su Misil:");
            int x = sn.nextInt();
            
            //Validaciones para el usuario 1
            if(usuario2.getTablerin().despliegaMisil(y , x) == 2){
                
                System.out.println("\n KABUUM \n");
                usuario1.getTableroDisparo().imprimeGolpe(x, y);
                usuario2.getTablerin().imprimeGolpe(x, y);
                System.out.println("\nLISTA DE BARCOS HUNDIDOS QUE HEMOS HUNDIDO CAPITÁN(A)\n");
                for(int i = 0; i < usuario2.getCoordenadasBarcos().size(); i++){
                    if (y==usuario2.getCoordenadasBarcos().get(i).getX() && x == usuario2.getCoordenadasBarcos().get(i).getY()) {
                        usuario2.setBarcosHundidos(usuario2.getCoordenadasBarcos().get(i));
                        barquitos.add(i);
                    }
                }

                if ((barquitos.contains(0))==true){
                        
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(0).getY(), usuario2.getCoordenadasBarcos().get(0).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(0).getY(), usuario2.getCoordenadasBarcos().get(0).getX());
        
                    System.out.println(" FRAGATA DESTRUIDA ");
                }
                if ((barquitos.contains(1))==true){
                        
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(1).getY(), usuario2.getCoordenadasBarcos().get(1).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(1).getY(), usuario2.getCoordenadasBarcos().get(1).getX());
                   
                    System.out.println(" FRAGATA DESTRUIDA ");
                }      

                
                if ((barquitos.contains(2)&& barquitos.contains(3))==true){
                    
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(2).getY(), usuario2.getCoordenadasBarcos().get(2).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(2).getY(), usuario2.getCoordenadasBarcos().get(2).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(3).getY(), usuario2.getCoordenadasBarcos().get(3).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(3).getY(), usuario2.getCoordenadasBarcos().get(3).getX());
               
                    System.out.println(" DESTRUCTOR DESTRUIDO ");


                
                }
                if ((barquitos.contains(4)&& barquitos.contains(5))==true){
                    
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(4).getY(), usuario2.getCoordenadasBarcos().get(4).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(4).getY(), usuario2.getCoordenadasBarcos().get(4).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(5).getY(), usuario2.getCoordenadasBarcos().get(5).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(5).getY(), usuario2.getCoordenadasBarcos().get(5).getX());
     
                    System.out.println(" DESTRUCTOR DESTRUIDO ");


                
                }
                if ((barquitos.contains(6)&& barquitos.contains(7))==true){
                    
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(6).getY(), usuario2.getCoordenadasBarcos().get(6).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(6).getY(), usuario2.getCoordenadasBarcos().get(6).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(7).getY(), usuario2.getCoordenadasBarcos().get(7).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(7).getY(), usuario2.getCoordenadasBarcos().get(7).getX());

                    System.out.println(" DESTRUCTOR DESTRUIDO ");


                
                }
                   
                if ((barquitos.contains(8)&& barquitos.contains(9) && barquitos.contains(10))==true){
                    
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(8).getY(), usuario2.getCoordenadasBarcos().get(8).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(8).getY(), usuario2.getCoordenadasBarcos().get(8).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(9).getY(), usuario2.getCoordenadasBarcos().get(9).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(9).getY(), usuario2.getCoordenadasBarcos().get(9).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(10).getY(), usuario2.getCoordenadasBarcos().get(10).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(10).getY(), usuario2.getCoordenadasBarcos().get(10).getX());
             
                    System.out.println(" SUBMARINO DESTRUIDO ");


                
                }
                if ((barquitos.contains(11)&& barquitos.contains(12) && barquitos.contains(13))==true){
                    
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(11).getY(), usuario2.getCoordenadasBarcos().get(11).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(11).getY(), usuario2.getCoordenadasBarcos().get(11).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(12).getY(), usuario2.getCoordenadasBarcos().get(12).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(12).getY(), usuario2.getCoordenadasBarcos().get(12).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(13).getY(), usuario2.getCoordenadasBarcos().get(13).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(13).getY(), usuario2.getCoordenadasBarcos().get(13).getX());

                    System.out.println(" SUBMARINO DESTRUIDO ");


                
                }
                if ((barquitos.contains(14)&& barquitos.contains(15) && barquitos.contains(16))==true){
                    
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(14).getY(), usuario2.getCoordenadasBarcos().get(14).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(14).getY(), usuario2.getCoordenadasBarcos().get(14).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(15).getY(), usuario2.getCoordenadasBarcos().get(15).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(15).getY(), usuario2.getCoordenadasBarcos().get(15).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(16).getY(), usuario2.getCoordenadasBarcos().get(16).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(16).getY(), usuario2.getCoordenadasBarcos().get(16).getX());
                  
                    System.out.println(" SUBMARINO DESTRUIDO ");


                
                }
                if ((barquitos.contains(17)&& barquitos.contains(18) && barquitos.contains(19) && barquitos.contains(20))==true){
                    
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(17).getY(), usuario2.getCoordenadasBarcos().get(17).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(17).getY(), usuario2.getCoordenadasBarcos().get(17).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(18).getY(), usuario2.getCoordenadasBarcos().get(18).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(18).getY(), usuario2.getCoordenadasBarcos().get(18).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(19).getY(), usuario2.getCoordenadasBarcos().get(19).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(19).getY(), usuario2.getCoordenadasBarcos().get(19).getX());
                    usuario1.getTableroDisparo().BarcoDown(usuario2.getCoordenadasBarcos().get(20).getY(), usuario2.getCoordenadasBarcos().get(20).getX());
                    usuario2.getTablerin().BarcoDown(usuario2.getCoordenadasBarcos().get(20).getY(), usuario2.getCoordenadasBarcos().get(20).getX());

              
                    System.out.println(" PORTAAVIONES DESTRUIDO ");


                
                }
                if (barquitos.size()==21) {
                    System.out.print("\033[H\033[2J");
                    System.out.flush();
                    System.out.println("\n\n\n GANA"+"   "+usuario1.getNombre()+"\n\n\n");
                    System.out.println("FELICIDADES");
                    System.exit(0);
        
                }
            
            }
                else if (usuario2.getTablerin().despliegaMisil(y,x)==1){
                
                System.out.println("\n AL AGUA PATO \n");
                usuario1.getTableroDisparo().imprimeAfuera(x, y);
                usuario2.getTablerin().imprimeAfuera(x, y);
                turno1=false;

            }

            else if (usuario2.getTablerin().despliegaMisil(y,x)==4){
                
                System.out.println("\n Ya atacamos ahí mi Capitán(a) \n");
            }
            else{

                System.out.println(" \n Despliega el misil en un terreno válido Capitán(a) \n");
            }
        }catch(InputMismatchException e){

            System.out.println("\nIntroduzca un número\n");

        }
        
        }
        //Para centrar la pantalla
        System.out.print("\033[H\033[2J");
        System.out.flush();
        int claveIngresada2=0;
        while (claveIngresada2 != usuario2.getClave()){
            Scanner sn = new Scanner(System.in);
            System.out.println("\n Turno de "+usuario2.getNombre()+"\n");
            try{
            System.out.println("Ingrese su contraseña númerica:");
            claveIngresada2 = sn.nextInt();
        }catch(InputMismatchException e){

            System.out.println("\nIntroduzca un número\n");

        }
        }
        boolean turno2 = true;
        while (turno2 == true){
            Scanner sn = new Scanner(System.in);
            System.out.println("\n"+usuario2.getNombre()+"\n");

            System.out.println(" Es tu turno Capitán(a): ");            
            
            System.out.println("¿Adónde disparamos? ");

            usuario2.getTablerin().imprimirOceano();
            System.out.println("\n");
            System.out.println("ZONA DE DISPAROS");
            usuario2.getTableroDisparo().imprimirOceano();
            try{
            System.out.println("Introduzca un x para desplegar su Misil:");
            int y = sn.nextInt();

            System.out.println("Introduzca un y para desplegar su Misil:");
            int x = sn.nextInt();


            if(usuario1.getTablerin().despliegaMisil(y, x) == 2){
                
                System.out.println("\n KABUUM \n");
                usuario2.getTableroDisparo().imprimeGolpe(x, y);
                usuario1.getTablerin().imprimeGolpe(x, y);
                System.out.println("\nLISTA DE BARCOS HUNDIDOS QUE HEMOS HUNDIDO CAPITÁN(A)\n");
                for(int i = 0; i < usuario1.getCoordenadasBarcos().size(); i++){
                    if (y==usuario1.getCoordenadasBarcos().get(i).getX() && x == usuario1.getCoordenadasBarcos().get(i).getY()) {
                        usuario1.setBarcosHundidos(usuario1.getCoordenadasBarcos().get(i));
                        barquitos2.add(i);
                    }
                }

                //Validaciones para el usuario 2
                if ((barquitos2.contains(0))==true){
                        
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(0).getY(), usuario1.getCoordenadasBarcos().get(0).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(0).getY(), usuario1.getCoordenadasBarcos().get(0).getX());
    
                    System.out.println(" FRAGATA DESTRUIDA ");
                    
                }
                if ((barquitos2.contains(1))==true){
                        
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(1).getY(), usuario1.getCoordenadasBarcos().get(1).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(1).getY(), usuario1.getCoordenadasBarcos().get(1).getX());
                 
                    System.out.println(" FRAGATA DESTRUIDA ");
                }      

                
                if ((barquitos2.contains(2)&& barquitos2.contains(3))==true){
                    
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(2).getY(), usuario1.getCoordenadasBarcos().get(2).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(2).getY(), usuario1.getCoordenadasBarcos().get(2).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(3).getY(), usuario1.getCoordenadasBarcos().get(3).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(3).getY(), usuario1.getCoordenadasBarcos().get(3).getX());
                
                    System.out.println(" DESTRUCTOR DESTRUIDO ");


                
                }
                if ((barquitos2.contains(4)&& barquitos2.contains(5))==true){
                    
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(4).getY(), usuario1.getCoordenadasBarcos().get(4).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(4).getY(), usuario1.getCoordenadasBarcos().get(4).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(5).getY(), usuario1.getCoordenadasBarcos().get(5).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(5).getY(), usuario1.getCoordenadasBarcos().get(5).getX());
               
                    System.out.println(" DESTRUCTOR DESTRUIDO ");


                
                }
                if ((barquitos2.contains(6)&& barquitos2.contains(7))==true){
                    
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(6).getY(), usuario1.getCoordenadasBarcos().get(6).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(6).getY(), usuario1.getCoordenadasBarcos().get(6).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(7).getY(), usuario1.getCoordenadasBarcos().get(7).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(7).getY(), usuario1.getCoordenadasBarcos().get(7).getX());
                  
                    System.out.println(" DESTRUCTOR DESTRUIDO ");


                
                }
                   
                if ((barquitos2.contains(8)&& barquitos2.contains(9) && barquitos2.contains(10))==true){
                    
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(8).getY(), usuario1.getCoordenadasBarcos().get(8).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(8).getY(), usuario1.getCoordenadasBarcos().get(8).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(9).getY(), usuario1.getCoordenadasBarcos().get(9).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(9).getY(), usuario1.getCoordenadasBarcos().get(9).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(10).getY(), usuario1.getCoordenadasBarcos().get(10).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(10).getY(), usuario1.getCoordenadasBarcos().get(10).getX());
               
                    System.out.println(" SUBMARINO DESTRUIDO ");


                
                }
                if ((barquitos2.contains(11)&& barquitos2.contains(12) && barquitos2.contains(13))==true){
                    
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(11).getY(), usuario1.getCoordenadasBarcos().get(11).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(11).getY(), usuario1.getCoordenadasBarcos().get(11).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(12).getY(), usuario1.getCoordenadasBarcos().get(12).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(12).getY(), usuario1.getCoordenadasBarcos().get(12).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(13).getY(), usuario1.getCoordenadasBarcos().get(13).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(13).getY(), usuario1.getCoordenadasBarcos().get(13).getX());
                  
                    System.out.println(" SUBMARINO DESTRUIDO ");


                
                }
                if ((barquitos2.contains(14)&& barquitos2.contains(15) && barquitos2.contains(16))==true){
                    
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(14).getY(), usuario1.getCoordenadasBarcos().get(14).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(14).getY(), usuario1.getCoordenadasBarcos().get(14).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(15).getY(), usuario1.getCoordenadasBarcos().get(15).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(15).getY(), usuario1.getCoordenadasBarcos().get(15).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(16).getY(), usuario1.getCoordenadasBarcos().get(16).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(16).getY(), usuario1.getCoordenadasBarcos().get(16).getX());
                    
                    System.out.println(" SUBMARINO DESTRUIDO ");


                
                }
                if ((barquitos2.contains(17)&& barquitos2.contains(18) && barquitos2.contains(19) && barquitos2.contains(20))==true){
                    
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(17).getY(), usuario1.getCoordenadasBarcos().get(17).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(17).getY(), usuario1.getCoordenadasBarcos().get(17).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(18).getY(), usuario1.getCoordenadasBarcos().get(18).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(18).getY(), usuario1.getCoordenadasBarcos().get(18).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(19).getY(), usuario1.getCoordenadasBarcos().get(19).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(19).getY(), usuario1.getCoordenadasBarcos().get(19).getX());
                    usuario2.getTableroDisparo().BarcoDown(usuario1.getCoordenadasBarcos().get(20).getY(), usuario1.getCoordenadasBarcos().get(20).getX());
                    usuario1.getTablerin().BarcoDown(usuario1.getCoordenadasBarcos().get(20).getY(), usuario1.getCoordenadasBarcos().get(20).getX());

                   
                    System.out.println(" PORTAAVIONES DESTRUIDO ");


                
                }
                if (barquitos2.size()==21) {
                    System.out.print("\033[H\033[2J");
                    System.out.flush();
                    System.out.println("\n\n\n GANA"+"   "+usuario2.getNombre()+"\n\n\n");
                    System.out.println("FELICIDADES");
                    //Finaliza el programa
                    System.exit(0);
                    
                }
            }
            else if (usuario1.getTablerin().despliegaMisil(y,x)==1){
                
                System.out.println("\n AL AGUA PATO \n");
                usuario2.getTableroDisparo().imprimeAfuera(x, y);
                usuario1.getTablerin().imprimeAfuera(x, y);
                turno2=false;

            }
            else if (usuario2.getTablerin().despliegaMisil(y,x)==4){
                
                System.out.println("\n Ya atacamos ahí mi Capitán(a) \n");
            }
            else{

                System.out.println(" \n Despliega el misil en un terreno válido Capitán(a) \n");
            }
        }catch(InputMismatchException e){

            System.out.println("\nIntroduzca un número\n");

        }
        }
    }

    




}