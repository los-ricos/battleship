/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

import java.util.ArrayList;
public class Barco{
    private String nombre;
    private boolean orientacion;
    private int tamanno;
    private ArrayList<Coordenada> coordenadasBarco = new ArrayList<Coordenada>();

/*-----------------------------------------------------------------------
	Barco
	Entradas: No posee
    Salidas: No posee
    Restricciones: No posee
	Funcionamiento: 
        - Constructor vacio con valores predeterminados
-----------------------------------------------------------------------*/
    public Barco(){
        nombre = " ";
        orientacion = false;
        tamanno = 0;

    }

/*-----------------------------------------------------------------------
	Barco
	Entradas: un string y un entero
    Salidas: No posee
    Restricciones: debe ser un string y un entero
	Funcionamiento: 
        - Constructor con parametros que los asigna al objeto
-----------------------------------------------------------------------*/
    public Barco(String nombre,int tamanno){
        this.nombre = nombre;
        this.tamanno = tamanno;
    }

/*-----------------------------------------------------------------------
	getNombre
	Entradas: No posee
    Salidas: Un string
    Restricciones: No posee
	Funcionamiento: 
        - Getter de nombre
-----------------------------------------------------------------------*/
    public String getNombre() {
        return this.nombre;
    }
    
/*-----------------------------------------------------------------------
	setNombre
	Entradas: Un string
    Salidas: No posee
    Restricciones: Debe ser string
	Funcionamiento: 
        - Setter de nombre
-----------------------------------------------------------------------*/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

/*-----------------------------------------------------------------------
	getOrientacion
	Entradas: No posee
    Salidas: Un booleano
    Restricciones: No posee
	Funcionamiento: 
        - Getter de orientacion
-----------------------------------------------------------------------*/
    public boolean getOrientacion(){
        return this.orientacion;

/*-----------------------------------------------------------------------
	setOrientacion
	Entradas: Un booleano
    Salidas: No posee
    Restricciones: Debe ser un booleano
	Funcionamiento: 
        - Setter de orientacion
-----------------------------------------------------------------------*/
    }
    public void setOrientacion(boolean orientacion){
        this.orientacion=orientacion;
    }

/*-----------------------------------------------------------------------
	getTamanno
	Entradas: No posee
    Salidas: Un entero
    Restricciones: No posee
	Funcionamiento: 
        - Getter de tamanno
-----------------------------------------------------------------------*/
    public int getTamanno(){
        return tamanno;
    }
/*-----------------------------------------------------------------------
	setTamanno
	Entradas: Un entero
    Salidas: No posee
    Restricciones: Debe ser un entero
	Funcionamiento: 
        - Setter de tamanno
-----------------------------------------------------------------------*/
    public void setTamanno(int tamanno){
        this.tamanno = tamanno;
    }

/*-----------------------------------------------------------------------
	insertarCoordenada
	Entradas: Un objeto de tipo coordenada
    Salidas: No posee
    Restricciones: Debe ser una coordenada
	Funcionamiento: 
        - Inserta una coordenada al arrayList
-----------------------------------------------------------------------*/
    public void insertarCoordenada(Coordenada coordenada){
        coordenadasBarco.add(coordenada);
    }

/*-----------------------------------------------------------------------
	eliminarCoordenada
	Entradas: Un objeto de tipo coordenada
    Salidas: No posee
    Restricciones: Debe ser una coordenada
	Funcionamiento: 
        - Elimina una coordenada del arrayList
-----------------------------------------------------------------------*/
    public void eliminarCoordenada(Coordenada coordenada){
        coordenadasBarco.remove(coordenada);;
    }

/*-----------------------------------------------------------------------
	obtenerCoordenada
	Entradas: Un entero
    Salidas: No posee
    Restricciones: Debe ser un entero
	Funcionamiento: 
        - Obtiene un objeto del ArrayList según su posicion
-----------------------------------------------------------------------*/
    public Object obtenerCoordenada(int pos){
      return coordenadasBarco.get(pos);
    }

/*-----------------------------------------------------------------------
	obtenerX
	Entradas: Un entero
    Salidas: No posee
    Restricciones: Debe ser un entero
	Funcionamiento: 
        - Obtiene un objeto del ArrayList según su posicion
-----------------------------------------------------------------------*/
    public int obtenerX(int pos){
        Coordenada coordenada = coordenadasBarco.get(pos);
        return coordenada.getX();
      }

/*-----------------------------------------------------------------------
	obtenerY
	Entradas: Un entero
    Salidas: No posee
    Restricciones: Debe ser un entero
	Funcionamiento: 
        - Obtiene un objeto del ArrayList según su posicion
-----------------------------------------------------------------------*/
    public int obtenerY(int pos){
        Coordenada coordenada = coordenadasBarco.get(pos);
        return coordenada.getY();
      }

/*-----------------------------------------------------------------------
	esVacia
	Entradas: No posee
    Salidas: Un booleano
    Restricciones: No posee
	Funcionamiento: 
        - Revisa si el arrayList se encuentra vacio
-----------------------------------------------------------------------*/
    public boolean esVacia(){
        return coordenadasBarco.isEmpty();  
    }

/*-----------------------------------------------------------------------
	tamañoLista
	Entradas: No posee
    Salidas: Un entero
    Restricciones: No posee
	Funcionamiento: 
        - Retorna el tamaño actual del arrayList
-----------------------------------------------------------------------*/
    public int tamañoLista(){
      return coordenadasBarco.size();
    }
}